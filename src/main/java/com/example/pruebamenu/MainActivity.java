package com.example.pruebamenu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView text1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text1 = findViewById(R.id.text1);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menudeopciones,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int opt = item.getItemId();
        float valor;

        switch (opt){
            case R.id.agrandarfuente:
                valor = text1.getTextSize();
                valor = valor + 30;
                text1.setTextSize(TypedValue.COMPLEX_UNIT_PX, valor);
                return true;

            case R.id.reducirfuente:
                valor = text1.getTextSize();
                valor = valor - 30;
                text1.setTextSize(TypedValue.COMPLEX_UNIT_PX, valor);
                return true;

            case R.id.salir:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}